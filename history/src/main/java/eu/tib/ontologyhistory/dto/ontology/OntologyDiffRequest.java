package eu.tib.ontologyhistory.dto.ontology;

import lombok.Getter;

@Getter
public class OntologyDiffRequest {
    private String idLeft;
    private String idRight;

}

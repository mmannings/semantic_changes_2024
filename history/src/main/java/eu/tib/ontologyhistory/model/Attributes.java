package eu.tib.ontologyhistory.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Attributes implements Serializable {

    private String attributeName;

}

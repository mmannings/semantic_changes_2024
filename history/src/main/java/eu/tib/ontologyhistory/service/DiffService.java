package eu.tib.ontologyhistory.service;

import com.google.common.collect.Sets;
import eu.tib.ontologyhistory.dto.diff.DiffAdd;
import eu.tib.ontologyhistory.dto.diff.DiffAndApiError;
import eu.tib.ontologyhistory.dto.diff.DiffDto;
import eu.tib.ontologyhistory.mapper.DiffMapper;
import eu.tib.ontologyhistory.model.ApiError;
import eu.tib.ontologyhistory.model.Axiom;
import eu.tib.ontologyhistory.model.Diff;
import eu.tib.ontologyhistory.model.exception.RobotDiffExecutionException;
import eu.tib.ontologyhistory.model.exception.UnloadableCustomImportException;
import eu.tib.ontologyhistory.repository.DiffRepository;
import eu.tib.ontologyhistory.service.network.GithubService;
import eu.tib.ontologyhistory.utils.ExceptionUtils;
import eu.tib.ontologyhistory.utils.FileUtils;
import eu.tib.ontologyhistory.utils.OntologyUtils;
import eu.tib.ontologyhistory.utils.ParserUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.bson.Document;
import org.geneontology.owl.differ.Differ;
import org.geneontology.owl.differ.render.BasicDiffRenderer;
import org.geneontology.owl.differ.render.MarkdownGroupedDiffRenderer;
import org.obolibrary.robot.CommandState;
import org.obolibrary.robot.DiffCommand;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologySetProvider;
import org.semanticweb.owlapi.model.UnloadableImportException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.Serial;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.*;


@Slf4j
@AllArgsConstructor
@Service
public class DiffService {

    private static final String OUTPUT_DIFF_FILENAME = "outputDiff.txt";

    private static final String DEFAULT_ONTOLOGY_ID = "default";

    private static final String MARKDOWN_DOCUMENT_KEY = "file";


    private final DiffRepository diffRepository;

    private final DiffMapper diffMapper;

    private final GithubService githubService;

    public List<DiffDto> findAll() {
        val diff = diffRepository.findAll();
        return diffMapper.entityToDto(diff);
    }

    public DiffDto findById(String id) {
        val diff = diffRepository.findById(id).orElse(null);
        return diffMapper.entityToDto(diff);
    }

    public void deleteById(String id) {
        diffRepository.deleteById(id);
    }

    private static class DualOntologySetProvider implements OWLOntologySetProvider {

        @Serial
        private static final long serialVersionUID = -8942374248162307075L;
        private final Set<OWLOntology> ontologies = Sets.newIdentityHashSet();

        /**
         * Init a new DualOntologySetProvider for a left and right ontology.
         *
         * @param left OWLOntologySetProvider for left ontology
         * @param right OWLOntologySetProvider for right ontology
         */
        public DualOntologySetProvider(OWLOntologySetProvider left, OWLOntologySetProvider right) {
            ontologies.addAll(left.getOntologies());
            ontologies.addAll(right.getOntologies());
        }

        /**
         * Get the ontologies in the provider.
         *
         * @return Set of OWLOntologies
         */
        @Nonnull
        @Override
        public Set<OWLOntology> getOntologies() {
            return Collections.unmodifiableSet(ontologies);
        }
    }

    private void diffExecute(DiffAdd diffAdd, File output) {
        val diffCommand = new DiffCommand();
        try {
            diffCommand.execute(new CommandState(), new String[]
                    {
                            "--left-iri", diffAdd.gitUrlLeft(),
                            "--right-iri", diffAdd.gitUrlRight(),
                            "--output", output.getName(),
                            "--format", "markdown"
                    });
        } catch (Exception e) {
            val throwable = ExceptionUtils.findRootCause(e);
            if (!ExceptionUtils.handleCustomException(throwable)) {
                throw new RobotDiffExecutionException("Some general error happened during diff execution");
            }
        }

    }

    private DiffDto diffDtoBuilder(DiffAdd diffAdd, Document markdown) {
        return DiffDto.builder()
                .timestamp(diffAdd.commitDate())
                .sha(diffAdd.sha())
                .parentSha(diffAdd.parentSha())
                .parentOffsetDateTime(diffAdd.parentOffsetDateTime())
                .shaOffsetDateTime(diffAdd.shaOffsetDateTime())
                .message(diffAdd.message())
                .markdown(markdown)
                .build();
    }

    private ApiError apiErrorBuilder(DiffAdd diffAdd) {
        return ApiError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .message("Some error happened during diff creation")
                .timestamp(diffAdd.commitDate())
                .leftIriFile(diffAdd.gitUrlLeft())
                .rightIriFile(diffAdd.gitUrlRight())
                .build();
    }

    public DiffAndApiError makeDiffExternal(String url, Instant datetime) {
        val diffAdds = githubService.getDiffAdds(url, datetime);
        val diffs = new ArrayList<DiffDto>();
        val apiErrors = new ArrayList<ApiError>();
            for (val diffAdd : diffAdds) {
                val outputDiff = new File(OUTPUT_DIFF_FILENAME);
                try {
                    diffExecute(diffAdd, outputDiff);

                    String line = Files.readString(outputDiff.toPath(), StandardCharsets.UTF_8);
                    Document markdown = new Document().append(MARKDOWN_DOCUMENT_KEY, line);
                    val diff = diffDtoBuilder(diffAdd, markdown);
                    diffs.add(diff);
                } catch (Exception e) {
                    val apiError = apiErrorBuilder(diffAdd);
                    apiErrors.add(apiError);
                }
            }
            return new DiffAndApiError(diffs, apiErrors);
    }

    public DiffDto makeDiffFromGit(DiffAdd diffAdd) {
        String ontologyLeftFilename = "ontology-left";
        String ontologyRightFilename = "ontology-right";

        Path diffOutputPlainFile = Path.of("diff-output-plain.txt");
        Path diffOutputPlainMarkdown = Path.of("diff-output-markdown.md");

        try {
            File ontLeft = FileUtils.createTempFile(ontologyLeftFilename, diffAdd.gitRawFileLeft());
            File ontRight = FileUtils.createTempFile(ontologyRightFilename, diffAdd.gitRawFileRight());

            OWLOntology loadedOntologyLeft = OntologyUtils.loadOntology(ontLeft);
            OWLOntology loadedOntologyRight = OntologyUtils.loadOntology(ontRight);

            OWLOntologySetProvider ontologySetProvider = new DualOntologySetProvider(
                    loadedOntologyLeft.getOWLOntologyManager(),
                    loadedOntologyRight.getOWLOntologyManager()
            );

            Differ.BasicDiff differ = Differ.diff(loadedOntologyLeft, loadedOntologyRight);
            Differ.GroupedDiff groupedForMarkdown = Differ.groupedDiff(differ);

            Files.write(diffOutputPlainFile, BasicDiffRenderer.renderPlain(differ).getBytes());
            Files.write(diffOutputPlainMarkdown, MarkdownGroupedDiffRenderer.render(groupedForMarkdown, ontologySetProvider).getBytes());


            List<String> lines = Files.readAllLines(diffOutputPlainFile, StandardCharsets.UTF_8);
            String line = Files.readString(diffOutputPlainMarkdown, StandardCharsets.UTF_8);

            Map<String, List<Axiom>> axioms = ParserUtils.parseAxioms(lines);

            Document markdown = new Document().append(MARKDOWN_DOCUMENT_KEY, line);

            val diff = Diff.builder()
                    .ontologyId(DEFAULT_ONTOLOGY_ID)
                    .markdown(markdown)
                    .timestamp(diffAdd.commitDate())
                    .sha(diffAdd.sha())
                    .parentSha(diffAdd.parentSha())
                    .parentOffsetDateTime(diffAdd.parentOffsetDateTime())
                    .shaOffsetDateTime(diffAdd.shaOffsetDateTime())
                    .axioms(axioms)
                    .message(diffAdd.message())
                    .build();

            if (diff != null) {
                val savedDiff = diffRepository.insert(diff);
                return diffMapper.entityToDto(savedDiff);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Error happened during diff creation: " + e.getMessage());
        }
        return null;
    }

    public void assignOntologyId(List<Diff> diffIds, String ontologyId) {
        for (Diff d : diffIds) {
            Diff diff = diffRepository.findById(d.getId()).orElse(null);
            if (diff != null) {
                diff.setOntologyId(ontologyId);
                diffRepository.save(diff);
            }
        }
    }

}

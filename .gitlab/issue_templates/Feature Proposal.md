### Proposal 

<!-- How are we going to solve the problem? -->

### Problem to solve 

<!-- What problem do we solve? -->

### Further details

<!-- Include use cases, benefits, goals, or any other details that will help us understand the problem better. -->
